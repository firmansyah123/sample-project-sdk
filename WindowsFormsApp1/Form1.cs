﻿using CashlezWindowsSdk.Companion;
using CashlezWindowsSdk.Companion.Printer;
using CashlezWindowsSdk.Companion.Reader;
using CashlezWindowsSdk.Login;
using Domain.Model.Login;
using Domain.Model.Paymentdetail;
using System;
using Timer = System.Timers.Timer;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CashlezWindowsSdk.Card;
using Domain.Model.Card.Request;
using Domain.model.paymentdetail;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form, ILoginListener, IReaderObserver, IPrinterObserver, ICardPaymentListener
    {
        private readonly ILoginHandler _loginHandler;
        //private Timer _checkTaskTimer;
        private readonly ICardPaymentHandler _cardPaymentHandler;

        public Form1()
        {
            InitializeComponent();

            var printerHandler = new PrinterHandler();
            printerHandler.RegisterPrinterObserver(this);

            var readerHandler = new ReaderHandler();
            readerHandler.RegisterReaderObserver(this);

            _loginHandler = new LoginHandler();
            _cardPaymentHandler = new CardPaymentHandler();
        }

        public void OnApplicationExpired(ErrorResponse errorResponse)
        {
            throw new NotImplementedException();
        }

        public void OnLoginError(ErrorResponse errorResponse)
        {
            MessageBox.Show("FAILED LOGIN");
        }

        public void OnLoginSuccess(LoginResponse loginResponse)
        {
            var companionHandler = new ReaderHandler();
            companionHandler.StartReaderConnection();
            //ConnectionStatus connectionStatus = companionHandler.ConnectionStatus();
            //LblReaderStatus.Text = "Connection Status: " + connectionStatus;
            MessageBox.Show("SUCCESS LOGIN");
        }

        public void OnNewVersionAvailable(ErrorResponse errorResponse)
        {
            throw new NotImplementedException();
        }

        public void OnPrinterError(string message)
        {
            throw new NotImplementedException();
        }

        public void OnReaderError(string message)
        {
            throw new NotImplementedException();
        }

        public void OnStartActivation(string mobileUpdateUrl)
        {
            throw new NotImplementedException();
        }

        public void UpdatePrinterConnection(ConnectionStatus connectionStatus)
        {
            throw new NotImplementedException();
        }

        public void UpdateReaderConnection(ConnectionStatus connectionStatus)
        {
            MethodInvoker inv = delegate { LblReaderStatus.Text = $"Connection status: {connectionStatus}"; };
            Invoke(inv);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _loginHandler.DoLogin(txtUsername.Text, txtPassword.Text, this);
        }

        private void btnCardPayment_Click(object sender, EventArgs e)
        {
            _cardPaymentHandler.ProceedInternationalCardPayment(new InternationalCardRequest
            {
                Amount = txtAmount.Text,
                Description = "Test",
                Image = "TEST"
            }, this);
        }

        public void OnCardPaymentSuccess(CardPaymentDetailResponse paymentDetailResponse)
        {
            MessageBox.Show(paymentDetailResponse.ApprovalCode);
        }

        public void OnCardPaymentError(ErrorResponse errorResponse)
        {
            MessageBox.Show("Payment Error : " + errorResponse.ErrorCode + " " + errorResponse.ErrorMessage +
                    "\n" + errorResponse.HostErrorCode + " " + errorResponse.HostErrorMessage);
        }

        public void OnPaymentTimeOut(ErrorResponse errorResponse)
        {
            throw new NotImplementedException();
        }

        public void OnSignatureTimeout(ErrorResponse errorResponse)
        {
            throw new NotImplementedException();
        }

        public void OnConfirmPaymentRequest(string confirmPaymentMessage)
        {
            throw new NotImplementedException();
        }

        public void OnCardPaymentStep(CardPaymentStep cardPaymentStep)
        {
            throw new NotImplementedException();
        }

        public void OnConfirmTransfer(TransferPaymentDetailResponse transferPaymentDetailResponse)
        {
            throw new NotImplementedException();
        }

        public void OnConfirmPoint(CreditWithPointDetail transferPaymentDetailResponse)
        {
            throw new NotImplementedException();
        }

        public void OnCardRemoved()
        {
            Console.WriteLine("Card Removed");
        }
    }
}
