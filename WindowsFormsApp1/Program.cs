﻿using CashlezWindowsSdk.Card;
using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Register payment service
            var paymentService = PaymentService.Instance;

            Application.Run(new Form1());
        }
    }
}
